<!-- page.tpl.php -->

<div id="page" class=" <?php print $classes; ?>"<?php print $attributes; ?>>

  <header id="header" class="clearfix" role="banner">

    <?php if ($site_name || $site_slogan || $logo): ?>
      <div id="logo-name-slogan" class="header-left-column  <?php if (!$page['header']) print "full-width "; ?> clearfix">
        <hgroup>
        
          <?php if ($logo): ?>
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
              <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
            </a>
          <?php endif; ?>

          <?php if ($site_name): ?>
            <div id="site-name">
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
            </div>
          <?php endif; ?>

          <?php if ($site_slogan): ?>
            <div id="site-slogan"><?php print $site_slogan; ?></div>
          <?php endif; ?>

        </hgroup>
      </div> <!-- /#logo-name-slogan -->
    <?php endif; ?>

    <?php if ($page['header']): ?>
      <div id="header-block-region" class="header-right-column">
        <?php print render($page['header']); ?>
      </div>
    <?php endif; ?>

  </header> <!-- /#header -->

	<nav id="nav-search" class="menu clearfix ">
  
		<?php if ($page['navbar_left']): ?>
			<div class="nav-search-left-column">
        <?php print render($page['navbar_left']); ?>
      </div>
		<?php endif; ?>  
  
		<?php if (!$page['navbar_left']): ?>
      <div class="nav-search-left-column">
        <?php
          $pid = variable_get('menu_main_links_source', 'main-menu');
          $tree = menu_tree($pid);
          $tree = str_replace(' class="nav-search-left-column main-menu clearfix"', '', $tree);
          $main_menu = drupal_render($tree);
          print "<div id='main-menu-title'><div class='inner'>Main menu</div></div>" . $main_menu; 
        ?>
      </div>
		<?php endif; ?>  
    
		<?php if ($page['navbar_right']): ?>
			<div class="nav-search-right-column"><?php print render($page['navbar_right']); ?></div>
		<?php endif; ?>
    
	</nav>

  <div id="main" class="clearfix">
    <div id="content" class="column center">
      
			<div id="content-inner" class="inner">
      <section role="main">
				
				<?php if ($breadcrumb): ?>
					<div class="breadcrumb"><?php print $breadcrumb; ?></div>
				<?php endif; ?>

        <?php if ( $title|| $messages || $tabs || $action_links): ?>
          <div id="content-header">

            <?php print $messages; ?>
            <?php print render($page['help']); ?>

            <?php if ($tabs_rendered = render($tabs)): ?>
              <div class="tabs"><?php print render($tabs); ?></div>
            <?php endif; ?>

            <?php if ($action_links): ?>
              <ul class="action-links"><?php print render($action_links); ?></ul>
            <?php endif; ?>


            <?php if ($title && !$is_front): ?> <!-- hides page title on front page -->
              <h1 class="title"><?php print $title; ?></h1>
            <?php endif; ?>
            
          </div> <!-- /#content-header -->
        <?php endif; ?>

        <div id="content-area">
          <?php print render($page['content']) ?>
        </div>

        <?php print $feed_icons; ?>

      </section>
      </div>
    </div> <!-- /content-inner /content -->

    <?php if ($page['sidebar_first']): ?>
      <div id="sidebar-first" class="column sidebar first">
        <div id="sidebar-first-inner" class="inner">
          <?php print render($page['sidebar_first']); ?>
        </div>
      </div>
    <?php endif; ?> <!-- /sidebar-first -->

    <?php if ($page['sidebar_second']): ?>
      <div id="sidebar-second" class="column sidebar second">
        <div id="sidebar-second-inner" class="inner">
          <?php print render($page['sidebar_second']); ?>
        </div>
      </div>
    <?php endif; ?> <!-- /sidebar-second -->

  </div> <!-- /main -->

</div> <!-- /page -->

<div id="footer">
  <footer>
	  <?php print render($page['footer']); ?>
  </footer>
</div> <!-- /footer -->

<div id="resolution">
  <div class="resolution smartphone-portrait">smartphone-portrait, 0 to 320</div>
  <div class="resolution smartphone-landscape">smartphone-landscape, 320 to 600 </div>
  <div class="resolution tablet-portrait">tablet-portrait, 600 to 768 </div>
  <div class="resolution tablet-landscape">tablet-landscape, 768 to 1024 </div>
  <div class="resolution small-monitors">small-monitors, 1024 to 1440 </div>
  <div class="resolution big-monitors">big-monitors, 1440 + </div>
</div>