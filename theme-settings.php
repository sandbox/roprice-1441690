<?php

 /**
  * Implements responsive_form_system_theme_settings_alter().
  *
  */

function responsive_form_system_theme_settings_alter(&$form, &$form_state) {

  // move breadcrumb options to top of settings page. 
	$form['breadcrumb'] = array(
		'#type' => 'fieldset',
		'#title' => t('Breadcrumb Settings'),
		'#weight' => '-50',
	);
  // allow users to toggle breadcrumb off/on.
	$form['breadcrumb']['breadcrumb_display'] = array(
		'#type' => 'checkbox',
		'#title' => t('Display breadcrumb'),
		'#default_value' => theme_get_setting('breadcrumb_display'),
	);
  // allow users to set breadcrumb separator.
	$form['breadcrumb']['breadcrumb_separator'] = array(
		'#type'  => 'textfield',
		'#title' => t('Breadcrumb separator'),
		'#description' => t('Text only. Dont forget to include spaces.'),
		'#default_value' => theme_get_setting('breadcrumb_separator'),
		'#size' => 8,
		'#maxlength' => 10,
	);
}