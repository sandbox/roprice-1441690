jQuery(document).ready(function () {

	if ( jQuery("#block-user-login #edit-name").length>0 ) {
		jQuery("#block-user-login #edit-name").val("Username");
		jQuery("#block-user-login #edit-name").click(function () {	
			jQuery(this).css("color","#333");
			jQuery(this).val("");
		})		
	}

	if ( jQuery("#block-user-login #edit-pass").length>0 ) {
		jQuery("#block-user-login #edit-pass").val("Password");
		jQuery("#block-user-login #edit-pass").click(function () {	
			jQuery(this).css("color","#333");
			jQuery(this).val("");
		})		
	}

	if ( jQuery("#search-block-form .form-text").length>0 ) {
		jQuery("#search-block-form .form-text").val("Search this site..");
		jQuery("#search-block-form .form-text").click(function ()
		{	
			jQuery(this).css("color","#333");
			jQuery(this).val("");
		})		
	}

	if ( jQuery("#search-form .form-text").length>0 ) {
		jQuery("#search-form .form-text").focus();
	}

	if (jQuery("#main-menu-title").length>0) {
		jQuery("#main-menu-title").click(function () {
			jQuery("nav").toggleClass("expanded");
		})
	}
  
	if (jQuery("#block-system-main-menu h2").length>0) {
		jQuery("#block-system-main-menu h2").click(function () {
			jQuery("nav").toggleClass("expanded");
		})
	}
  
});

