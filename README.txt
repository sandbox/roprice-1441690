
ABOUT RESPONSIVE
--------------------
Responsive is a minimalist, mobile-friendly theme that supports responsive web design and development. That means that it looks good on all kinds of devices -- the Android phone, the iPad, the 27 inch Dell Studio Monitor, and so on. Same site, same content, same theme, looks good everywhere. 

It is meant to be ready to go out of the box but also easy to customize with a Responsive Web design. 

Read more about Responsive Web Design: http://www.alistapart.com/articles/responsive-web-design


ABOUT DRUPAL THEMING
--------------------
To learn how to build your own custom theme and override Drupal's default code,
see the Theming Guide: http://drupal.org/theme-guide

See the sites/all/themes/README.txt for more information on where to place your
custom themes to ensure easy maintenance and upgrades.


IE SUPPORT 
--------------------
Responsive is optimized for Windows Mobile IE browsers. It also caters to IE 9. It works fine on IE8 and below but doesn't have "responsive" properties -- the layout doesn't change as the window is resized. Instead, IE8 and below load the "mobile" version, where all content is stacked into a single column.