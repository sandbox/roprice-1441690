<?php

/**
 * Implements hook_form_FORM_ID_alter().
 * 
 * Replace Drupal defaults with better wording on login form.
 */
function responsive_form_user_login_block_alter(&$form) {
  $items = array();
  if (variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL)) {
    $items[] = l(t('Register'), 'user/register', array('attributes' => array('title' => t('Create a new user account.'))));
  }
  $items[] = l(t('Forgot password?'), 'user/password', array('attributes' => array('title' => t('If you forgot your password, click to request new password via e-mail.'))));
  $form['links'] = array('#markup' => theme('item_list', array('items' => $items),array('class' => 'clearfix')));
  // Set a weight for form actions so other elements can be placed beneath
  $form['actions']['#weight'] = 5;
  $form['links']['#weight'] = 10;
}

/**
 * Implements template_preprocess_block().
 */
function responsive_preprocess_block(&$vars, $hook) {
  // Add zebra stripes to blocks.
  $vars['classes_array'][] = 'block-' . $vars['zebra'];

  // Add first and last classes to blocks in each region
    
    if(!isset($counts)) $counts = array();
    $region = $vars['block']->region;
    if(!isset($counts[$region])) $counts[$region] = count(block_list($region));
    $count = $counts[$region];
    $extremity = '';
    if($vars['block_id'] == 1) $extremity = 'first';
    if($vars['block_id'] == $count) $extremity = 'last';
    
    array_push($vars['classes_array'], $extremity);   

    array_push($vars['classes_array'], 'blocks-in-region-'.$count);

    if ($counts[$region] == 1) array_push($vars['classes_array'], 'first last'); 

  // In the header region visually hide block titles.
  if ($vars['block']->region == 'header') {
    $vars['title_attributes_array']['class'][] = 'element-invisible';
  }
}

/**
 * Change the search form to use the "search" input element of HTML5
 */
function html5_boilerplate_preprocess_search_block_form(&$vars) {
  $vars['search_form'] = str_replace('type="text"', 'type="search"', $vars['search_form']);
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function responsive_breadcrumb($vars) {
  $breadcrumb = $vars['breadcrumb'];
  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('breadcrumb_display');
  if ($show_breadcrumb == 1) {

    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $separator = filter_xss(theme_get_setting('breadcrumb_separator'));
    
      // Just add the trailing separator      
			$trailing_separator = $separator;      

      // Assemble the breadcrumb
      return implode($separator, $breadcrumb) . $separator . drupal_get_title();
    }
  }
  // Otherwise, return an empty string.
  return '';
}

/**
 * Strip time stamps
 * 
 * For each of the default date formats, get rid of the time stamp that Drupal core appends on dates. 
 * admin interface will still display timestamp (admin/config/regional/date-time)
 */
variable_set('date_format_' . 'short', 'm/d/Y');
variable_set('date_format_' . 'medium', 'D, m/d/Y'); 
variable_set('date_format_' . 'long', 'F d, Y'); 